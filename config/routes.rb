Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :time_sheet_entries, only: %i[index create], path: 'time_entries'
end
