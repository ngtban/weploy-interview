class CreateTimeSheetEntries < ActiveRecord::Migration[6.1]
  def up
    create_table :time_sheet_entries do |t|
      t.datetime :start_time, null: false, default: -> { "now()" }
      t.datetime :end_time, null: false, default: -> { "now()" }
      t.decimal :value, null: false, default: 0, precision: 38, scale: 2

      t.datetime :created_at, null: false, default: -> { "now()" }
      t.datetime :updated_at, null: false, default: -> { "now()" }
    end

    # create an index to aid searching for overlapping ranges
    execute "create index entry_span on time_sheet_entries using gist (tsrange(start_time, end_time))"
  end

  def down
    drop_table :time_sheet_entries
  end
end
