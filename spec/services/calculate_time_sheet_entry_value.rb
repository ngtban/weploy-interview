RSpec.describe CalculateTimeSheetEntryValue do
  describe 'calculate for a pair of end time and start time' do
    let(:last_week) { DateTime.current.last_week }
    let(:acceptable_delta) { BigDecimal('0.01') }

    let(:mfw_within_business_hours_entry) {
      {
        start_time: last_week + CalculateTimeSheetEntryValue::MWF_START_BUSINESS_HOUR.hours + 1.hour,
        end_time: last_week + CalculateTimeSheetEntryValue::MWF_START_BUSINESS_HOUR.hours + 4.hours
      }
    }

    it 'returns the correct value when the time entry falls within MWF work hours' do
      value =
        subject.execute(**mfw_within_business_hours_entry)

      expected_value = CalculateTimeSheetEntryValue::MWF_NORMAL_RATE * BigDecimal(3)

      expect(value).to be_within(acceptable_delta).of(expected_value)
    end

    let(:mfw_overlapping_business_hours_entry) {
      wednesday = last_week + 2.days

      {
        start_time: wednesday + CalculateTimeSheetEntryValue::MWF_START_BUSINESS_HOUR.hours - 1.hour,
        end_time: wednesday + CalculateTimeSheetEntryValue::MWF_START_BUSINESS_HOUR.hours + 2.hours
      }
    }

    it 'returns the correct value when the time entry overlaps with MWF work hours' do
      value =
        subject.execute(**mfw_overlapping_business_hours_entry)

      expected_value =
        CalculateTimeSheetEntryValue::MWF_NORMAL_RATE * BigDecimal(2) +
          CalculateTimeSheetEntryValue::MWF_OT_RATE * BigDecimal(1)

      expect(value).to be_within(acceptable_delta).of(expected_value)
    end

    let(:mfw_outside_business_hours_entry) {
      friday = last_week + 4.days

      {
        start_time: friday + CalculateTimeSheetEntryValue::MWF_END_BUSINESS_HOUR.hours + 1.hour,
        end_time: friday + CalculateTimeSheetEntryValue::MWF_END_BUSINESS_HOUR.hours + 4.hours
      }
    }

    it 'returns the correct value when the time entry falls outside MWF work hours' do
      value =
        subject.execute(**mfw_outside_business_hours_entry)

      expected_value =
          CalculateTimeSheetEntryValue::MWF_OT_RATE * BigDecimal(3)

      expect(value).to be_within(acceptable_delta).of(expected_value)
    end

    let(:tt_within_business_hours_entry) {
      tuesday = last_week + 1.day

      {
        start_time: tuesday + CalculateTimeSheetEntryValue::TT_START_BUSINESS_HOUR.hours + 4.hour,
        end_time: tuesday + CalculateTimeSheetEntryValue::TT_START_BUSINESS_HOUR.hours + 8.hours
      }
    }

    it 'returns the correct value when the time entry falls within TT work hours' do
      value =
        subject.execute(**tt_within_business_hours_entry)

      expected_value =
        CalculateTimeSheetEntryValue::TT_NORMAL_RATE * BigDecimal(4)

      expect(value).to be_within(acceptable_delta).of(expected_value)
    end

    let(:tt_overlapping_business_hours_entry) {
      tuesday = last_week + 1.day

      {
        start_time: tuesday + CalculateTimeSheetEntryValue::TT_START_BUSINESS_HOUR.hours - 3.hour,
        end_time: tuesday + CalculateTimeSheetEntryValue::TT_START_BUSINESS_HOUR.hours + 2.hours
      }
    }

    it 'returns the correct value when the time entry overlaps with TT work hours' do
      value =
        subject.execute(**tt_overlapping_business_hours_entry)

      expected_value =
        CalculateTimeSheetEntryValue::TT_OT_RATE * BigDecimal(3) +
          CalculateTimeSheetEntryValue::TT_NORMAL_RATE * BigDecimal(2)

      expect(value).to be_within(acceptable_delta).of(expected_value)
    end

    let(:tt_outside_business_hours_entry) {
      thursday = last_week + 3.day

      {
        start_time: thursday + CalculateTimeSheetEntryValue::TT_START_BUSINESS_HOUR.hours - 4.hour,
        end_time: thursday + CalculateTimeSheetEntryValue::TT_START_BUSINESS_HOUR.hours
      }
    }

    it 'returns the correct value when the time entry falls outside TT work hours' do
      value =
        subject.execute(**tt_outside_business_hours_entry)

      expected_value =
        CalculateTimeSheetEntryValue::TT_OT_RATE * BigDecimal(4)

      expect(value).to be_within(acceptable_delta).of(expected_value)
    end

    let(:weekend_entry) {
      sunday = last_week + 6.days

      {
        start_time: sunday + 5.hours,
        end_time: sunday + 12.hours
      }
    }

    it 'returns the correct value when the time entry falls within weekends' do
      value =
        subject.execute(**weekend_entry)

      expected_value =
        CalculateTimeSheetEntryValue::WEEKEND_RATE * BigDecimal(7)

      expect(value).to be_within(acceptable_delta).of(expected_value)
    end
  end
end
