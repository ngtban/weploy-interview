FactoryBot.define do
  factory :time_sheet_entry do
    start_time { DateTime.current.yesterday }
    end_time { DateTime.current.yesterday + 2.hours }
  end
end
