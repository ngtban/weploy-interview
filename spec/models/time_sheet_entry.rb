RSpec.describe TimeSheetEntry do
  let(:yesterday) { DateTime.yesterday }

  describe 'validations' do
    it 'does not accept null start or end time' do
      entry = build(
        :time_sheet_entry,
        start_time: nil,
        end_time: DateTime.current
      )

      expect(entry.valid?).to be_falsey
      expect(entry.errors.messages.keys).to include(:start_time)

      entry = build(
        :time_sheet_entry,
        start_time: yesterday,
        end_time: nil
      )

      expect(entry.valid?).to be_falsey
      expect(entry.errors.messages.keys).to include(:end_time)
    end

    it 'does not accept end time in the future' do
      entry =
        build(
          :time_sheet_entry,
          start_time: DateTime.current,
          end_time: DateTime.current + 6.hours
        )

      expect(entry.valid?).to be_falsey
      expect(entry.errors.messages.keys).to include(:end_time)
    end

    it 'does not accept end time prior to start time' do
      entry =
        build(
          :time_sheet_entry,
          start_time: yesterday + 2.hours,
          end_time: yesterday + 1.hours
        )

      expect(entry.valid?).to be_falsey
      expect(entry.errors.messages.keys).to include(:start_time)
    end

    it 'does not accept start time and end time on different dates' do
      entry =
        build(
          :time_sheet_entry,
          start_time: yesterday,
          end_time: DateTime.current
        )

      expect(entry.valid?).to be_falsey
      expect(entry.errors.messages.keys).to include(:dates_of_entry)
    end

    it 'does not accept entries overlapping with existing ones' do
      list_existing_entry_data = [
        { start_time: yesterday + 2.hours, end_time: yesterday + 4.hours },
        { start_time: yesterday + 5.hours, end_time: yesterday + 7.hours },
        { start_time: yesterday + 8.hours, end_time: yesterday + 10.hours }
      ]

      list_existing_entry_data.each do |entry_data|
        create(:time_sheet_entry, **entry_data)
      end

      list_overlapping_entry_data = [
        { start_time: yesterday + 1.hours, end_time: yesterday + 3.hours },
        { start_time: yesterday + 4.hours, end_time: yesterday + 6.hours },
        { start_time: yesterday + 9.hours, end_time: yesterday + 9.hours + 20.minutes }
      ]

      entry_one, entry_two, entry_three =
        list_overlapping_entry_data.map do |entry_data|
          build(:time_sheet_entry, **entry_data)
        end

      expect(entry_one.valid?).to be_falsey
      expect(entry_one.errors.messages.keys).to include(:overlap)

      expect(entry_two.valid?).to be_falsey
      expect(entry_two.errors.messages.keys).to include(:overlap)

      expect(entry_three.valid?).to be_falsey
      expect(entry_three.errors.messages.keys).to include(:overlap)
    end
  end

  describe 'callbacks' do
    it 'calculates the value of the entry before creating it in the db' do
      entry =
        create(
          :time_sheet_entry,
          start_time: yesterday + 2.hours,
          end_time: yesterday + 3.hours
        )

      expect(entry.value).to_not eq(BigDecimal(0))
    end
  end
end
