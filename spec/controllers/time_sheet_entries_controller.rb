RSpec.describe TimeSheetEntriesController do
  let(:yesterday) { DateTime.yesterday }

  describe 'GET /time_entries' do
    let(:list_entry_data) {
      [
        { start_time: yesterday + 2.hours, end_time: yesterday + 4.hours },
        { start_time: yesterday + 5.hours, end_time: yesterday + 7.hours },
        { start_time: yesterday + 8.hours, end_time: yesterday + 10.hours }
      ]
    }

    it 'responds with a json object containing time entries when the format is json' do
      list_entry_data.each { |entry_data| create(:time_sheet_entry, **entry_data) }
 
      request.headers['Accept'] = 'application/json'

      post :index
      
      response_body = JSON.parse(response.body)

      expect(response_body.keys - %w[page per_page data]).to eq([])
      expect(response_body['data']).to_not eq([])
      expect(response_body['data'].first).to have_key("end_time")
    end
  end

  describe 'POST /time_entries' do
    let(:valid_entry_params) { { start_time: yesterday, end_time: yesterday + 2.hours }}
    let(:empty_start_time_params) { valid_entry_params.merge(start_time: nil) }

    it 'responds with status 200 when the data is valid' do
      post :create, params: valid_entry_params

      expect(response).to have_http_status(:ok)
    end

    it 'creates a new time entry when the data is valid' do
      expect(TimeSheetEntry.last).to be_nil

      post :create, params: valid_entry_params

      created_entry = TimeSheetEntry.last
      expect(created_entry).to_not be_nil
    end

    it 'responds with status 422 when the data is invalid' do
      post :create, params: empty_start_time_params

      expect(response).to have_http_status(:unprocessable_entity)
    end

    it 'does not create a new time entry when the data is invalid' do
      expect(TimeSheetEntry.last).to be_nil

      post :create, params: empty_start_time_params

      expect(TimeSheetEntry.last).to be_nil
    end

    it 'responds with a json object containing the errors when the data is invalid' do
      expect(TimeSheetEntry.last).to be_nil

      post :create, params: empty_start_time_params

      response_body = JSON.parse(response.body)
      expect(response_body).to have_key("errors")
    end
  end
end
