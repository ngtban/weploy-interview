class CalculateTimeSheetEntryValue
  DAY_FACTION_PRECISION = 2
  WEEKEND_RATE = BigDecimal(47)

  MWF_NORMAL_RATE = BigDecimal(22)
  MWF_OT_RATE = BigDecimal(34)
  MWF_START_BUSINESS_HOUR = 7
  MWF_END_BUSINESS_HOUR = 19

  TT_NORMAL_RATE = BigDecimal(25)
  TT_OT_RATE = BigDecimal(35)
  TT_START_BUSINESS_HOUR = 5
  TT_END_BUSINESS_HOUR = 17

  SECONDS_IN_MINUTES = 60
  MINUTES_IN_HOUR = 60
  SECONDS_IN_HOUR = SECONDS_IN_MINUTES * MINUTES_IN_HOUR

  def execute(start_time:, end_time:)
    # assumes that the bounds have already passed validations

    if start_time.on_weekend?
      time_diff = Rational(end_time.to_i - start_time.to_i, SECONDS_IN_HOUR)
      return WEEKEND_RATE * BigDecimal(time_diff, 2)
    end

    if start_time.monday? || start_time.wednesday? || start_time.friday?
      return calculate_for(
        start_time: start_time,
        end_time: end_time,
        start_business_hour: MWF_START_BUSINESS_HOUR,
        end_business_hour: MWF_END_BUSINESS_HOUR,
        normal_rate: MWF_NORMAL_RATE,
        ot_rate: MWF_OT_RATE
      )
    end

    if start_time.tuesday? || start_time.thursday?
      return calculate_for(
        start_time: start_time,
        end_time: end_time,
        start_business_hour: TT_START_BUSINESS_HOUR,
        end_business_hour: TT_END_BUSINESS_HOUR,
        normal_rate: TT_NORMAL_RATE,
        ot_rate: TT_OT_RATE
      )
    end

    BigDecimal(0)
  end

  # I haven't found anything that handles intersection of time ranges more gracefully
  # The logic for this would be more complex if the business hours do not start
  # exactly on minute 0, for example starting at 0530. The conditions used here
  # should check for the starting and ending minutes as well
  def calculate_for(
    start_time:,
    end_time:,
    start_business_hour:,
    end_business_hour:,
    normal_rate:,
    ot_rate:
  )
    normal_time = BigDecimal(0)
    ot_time = BigDecimal(0)

    start_of_business_hour = (start_time.beginning_of_day + start_business_hour.hours)
    end_of_business_hour = (start_time.beginning_of_day + end_business_hour.hours)

    if start_time.hour < start_business_hour && end_time.hour < end_business_hour
      ot_time_difference =
        Rational(start_of_business_hour.to_i - start_time.to_i, SECONDS_IN_HOUR)

      ot_time += BigDecimal(ot_time_difference, DAY_FACTION_PRECISION)

      normal_time_difference =
        Rational(end_time.to_i - start_of_business_hour.to_i, SECONDS_IN_HOUR)

      normal_time += BigDecimal(normal_time_difference, DAY_FACTION_PRECISION)

    elsif start_time.hour >= start_business_hour && end_time.hour < end_business_hour
      normal_time_difference = Rational(end_time.to_i - start_time.to_i, SECONDS_IN_HOUR)
      normal_time += BigDecimal(normal_time_difference, DAY_FACTION_PRECISION)

    elsif start_time.hour >= start_business_hour &&
      !(start_time.hour >= end_business_hour) &&
        end_time.hour >= end_business_hour

      normal_time_difference =
        Rational(end_of_business_hour.to_i - start_time.to_i, SECONDS_IN_HOUR)

      normal_time += BigDecimal(normal_time_difference, DAY_FACTION_PRECISION)

      ot_time_difference = Rational(end_time.to_i - end_of_business_hour.to_i)
      ot_time += BigDecimal(ot_time_difference, DAY_FACTION_PRECISION)

    elsif start_time.hour >= end_business_hour
      ot_time_difference = Rational(end_time.to_i - start_time.to_i, SECONDS_IN_HOUR)
      ot_time += BigDecimal(ot_time_difference, DAY_FACTION_PRECISION)
    end

    (normal_rate * normal_time + ot_rate * ot_time).round(2, :down)
  end
end
