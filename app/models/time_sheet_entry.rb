class TimeSheetEntry < ApplicationRecord
  attribute :start_time, :datetime
  attribute :end_time, :datetime
  attribute :value, :decimal

  validates :start_time, :end_time, presence: true

  validate :start_time_prior_to_end_time, if: :bounds_presence
  validate :end_time_not_in_the_future, if: :bounds_presence
  validate :within_same_day, if: :bounds_presence
  validate :no_overlap, if: -> { bounds_presence && end_time.after?(start_time) }

  before_create :calculate_value

  private

  def bounds_presence
    start_time && end_time
  end

  def start_time_prior_to_end_time
    if start_time.after?(end_time) || start_time.equal?(end_time)
      errors.add(:start_time, "must be prior to end time.")
    end
  end

  def end_time_not_in_the_future
    if DateTime.current.before?(end_time)
      errors.add(:end_time, "cannot be in the future.")
    end
  end

  def within_same_day
    if start_time.day != end_time.day
      errors.add(:dates_of_entry, "must be on the same day")
    end
  end

  def no_overlap
    overlap_exists =
      TimeSheetEntry.
        where("tsrange(start_time, end_time) && tsrange(?, ?)", start_time, end_time).
        exists?

    if overlap_exists
      errors.add(:overlap, "with existing entries must not occur.")
    end
  end

  def calculate_value
    if errors.empty?
      self[:value] = CalculateTimeSheetEntryValue.new.execute(start_time: start_time, end_time: end_time)
    end
  end
end
