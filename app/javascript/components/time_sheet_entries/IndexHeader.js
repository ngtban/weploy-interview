import React from 'react';
import { Button } from '@windmill/react-ui';

export default function IndexHeader({ onClickCreateNewEntry }) {
  return (
    <div>
      <Button onClick={onClickCreateNewEntry}>
        Create New Entry
      </Button>
    </div>
  );
}
