import React, { useState } from 'react';
import {
  Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Button
} from '@windmill/react-ui'

export default function NewEntryModal({ isOpen, onSaveEntry, onClose }) {
  const [date, setDate] = useState(new Date);
  const [startTime, setStartTime] = useState('');
  const [startTimeMax, setStartTimeMax] = useState('');
  const [finishTimeMin, setFinishTimeMin] = useState('');
  const [finishTime, setFinishTime] = useState('');

  const onDateChange = (event) => setDate(event.target.value);

  const onStartTimeChange = (event) => {
    setStartTime(event.target.value);
    setFinishTimeMin(event.target.value);
  };

  const onFinishTimeChange = (event) => {
    setFinishTime(event.target.value);
    setStartTimeMax(event.target.value)
  };

  const onClickSave = (_event) => {
    try {
      const startDate = new Date(date);
      const endDate = new Date(date);

      const [startHour, startMinute] = startTime?.split(':');
      startDate.setHours(Number(startHour));
      startDate.setMinutes(Number(startMinute));

      const [endHour, endMinute] = finishTime?.split(':');
      endDate.setHours(Number(endHour));
      endDate.setMinutes(Number(endMinute));

      if (!isNaN(startDate.getTime()) && !isNaN(endDate.getTime())) {
        onSaveEntry({ startDate, endDate });
        onClose();
      } else {
        alert(`Invalid date or time given, please check again.`)
      }
    } catch(error) {
      alert(`Invalid date or time given, details: ${error.message}`);
    }
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalHeader>Create a New Entry</ModalHeader>
      <ModalBody>
        <Label>Date</Label>
        <Input type='date' required onChange={onDateChange}/>
        <Label>Start</Label>
        <Input type='time' max={startTimeMax} required onChange={onStartTimeChange} />
        <Label>Finish</Label>
        <Input type='time' min={finishTimeMin} required onChange={onFinishTimeChange}/>
      </ModalBody>
      <ModalFooter>
        <Button onClick={onClickSave}>
          Save
        </Button>
      </ModalFooter>
    </Modal>
  );
}
