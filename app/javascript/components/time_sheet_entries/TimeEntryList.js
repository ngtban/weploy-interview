import React from 'react';

import {
  TableContainer, Table, TableHeader, TableBody, TableRow, TableCell
} from '@windmill/react-ui';

export default function TimeEntryList({ timeEntries }) {
  return(
   <TableContainer>
      <Table>
        <TableHeader >
        <TableRow>
          <TableCell>Duration</TableCell>
          <TableCell>Value</TableCell>
        </TableRow>
        </TableHeader>
        <TableBody>
        {
          timeEntries.map( timeEntry => {
            const startDate = new Date(timeEntry.start_time);
            const endDate = new Date(timeEntry.end_time);
            const startTime = `${startDate.getHours()}:${startDate.getMinutes()}`;
            const endTime = `${endDate.getHours()}:${endDate.getMinutes()}`;

            const duration = `${startDate.toLocaleDateString()} ${startTime} - ${endTime}`;

            return (
              <TableRow key={timeEntry.id}>
                <TableCell>{duration}</TableCell>
                <TableCell>${timeEntry.value}</TableCell>
              </TableRow>
            );
          })
        }
        </TableBody>
      </Table>
   </TableContainer>
  );
}
