import React, { useMemo, useState } from 'react';
import { render } from 'react-dom';
import {Windmill} from "@windmill/react-ui";

import IndexHeader from '../../components/time_sheet_entries/IndexHeader';
import NewEntryModal from '../../components/time_sheet_entries/NewEntryModal';
import TimeEntryList from '../../components/time_sheet_entries/TimeEntryList';

import { DEFAULT_REQUEST_HEADERS } from "../../utils/request_headers";

function TimeSheetEntriesIndex() {
  const [showNewEntryModal, setShowNewEntryModal] = useState(false);
  const [timeEntries, setTimeEntries] = useState([]);

  const fetchTimeEntries = () => {
    fetch('time_entries', { headers: DEFAULT_REQUEST_HEADERS }).
      then(raw_response => raw_response.json()).
      then(response_body => setTimeEntries(response_body.data)).
      catch(error =>
        alert(`An error happened while fetching time entries, details: ${error.message}`)
      )
  }

  const createNewEntry = ({ startDate, endDate }) => {
    fetch('time_entries', {
      method: 'POST',
      headers: DEFAULT_REQUEST_HEADERS,
      body: JSON.stringify({ start_time: startDate, end_time: endDate })
    }).
      catch(error =>
        alert(`An error happened while creating new time entries, details: ${error.message}`)
      )
  }

  useMemo(fetchTimeEntries, [])

  const onClickCreateNewEntry = () => setShowNewEntryModal(true);

  const onCloseNewEntryModal = () => setShowNewEntryModal(false);

  const onSaveNewEntry = (entryData) => {
    createNewEntry(entryData);

    fetchTimeEntries();
  };

  return (
    <Windmill>
      <IndexHeader {...{ onClickCreateNewEntry }}/>
      <NewEntryModal
        isOpen={showNewEntryModal}
        onClose={onCloseNewEntryModal}
        onSaveEntry={onSaveNewEntry}
      />
      <TimeEntryList {...{ timeEntries } }/>
    </Windmill>
  );
}

document.addEventListener('DOMContentLoaded', () => {
  render(
    <TimeSheetEntriesIndex />,
    document.getElementById('root')
  );
});
