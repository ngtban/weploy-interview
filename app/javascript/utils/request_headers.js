export const CSRF_TOKEN = document.querySelector("meta[name='csrf-token']").getAttribute('content');

export const DEFAULT_REQUEST_HEADERS =
  {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRF-Token': CSRF_TOKEN
  };
