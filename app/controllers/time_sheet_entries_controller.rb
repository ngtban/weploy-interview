class TimeSheetEntriesController < ApplicationController
  DEFAULT_PER_PAGE = 10
  DEFAULT_PAGE = 1

  def index
    respond_to do |format|
      format.html do
      end

      format.json do
        per_page = params['per_page'] || DEFAULT_PER_PAGE
        page = params['page'] || 1

        entries =
          TimeSheetEntry.
            order(start_time: :desc).
            limit(per_page).
            offset(per_page * (page - 1))

        response_data = {
          "page" => page,
          "per_page" => per_page,
          "data" => entries.map(&:as_json)
        }

        render status: :ok, json: response_data
      end
    end
  end

  def create
    permitted_attributes = %w[start_time end_time]

    entry_params = params.permit(permitted_attributes).slice(*permitted_attributes)

    entry = TimeSheetEntry.new(**entry_params)

    if entry.valid?
      entry.save

      render status: :ok, json: entry.as_json
    else
      render status: :unprocessable_entity, json: { errors: entry.errors.messages.as_json }
    end
  end
end
