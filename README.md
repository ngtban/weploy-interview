# README

## Setting up the project
Please follow the instructions in the subsections below to setup and get the project running.

### Prerequisites
Please install:
1. Ruby 3.0.2
2. Postgres 13.4

I cannot guarantee that the project would work with other Ruby or Postgres versions.

You might be able to run this project using an older version of Postgres, as long as that version supports GIST index and range types.

Please make sure that the Ruby version you use matches the above.

### Getting the project up and running

#### Installing dependencies

1. After step 4 above, run `bundle install` to fetch Rails dependencies.
2. Run `yarn install` to fetch front-end dependencies.

#### Add your own database configuration
I added `config/database.yml` to the list of ignored files so that whoever cloned this repository could use their own database configuration.

You will need to
1. Create a `database.yml` file within the `config` folder of the project
2. Create a database user with username and password matching the credentials used in the file above

For example, your `database.yml` file should look like this:

```yaml
default: &default
  adapter: postgresql
  encoding: unicode
  username: # your username
  password: # your password
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>

development:
  <<: *default
  database: # development database name

test:
  <<: *default
  database: # test database name

production:
  <<: *default
  database: # production database name
```

As for creating the database user:
1. Open a terminal
2. Login as `postgresql` to get admin access using `sudo su - postgres`
3. Run `psql` to open the psql cli
4. Run `create user <username> with password <password>;` with your username and password of choice.
5. Run `alter user <username> createdb;` to grant the user the right to create databases.

#### Initializing the databases
Once you have followed the steps above, run these commands in a terminal to let Rails setup the databases:

```bash
rails db:setup
rails db:migrate
```

Once the commands are finished the setup is complete.

To make sure that there are no issues, please:
1. Open a terminal
2. Start the Rails server with `rails s`
3. Open a browser and navigate to `localhost:3000`. You should see a Rails welcome screen.

### Running tests

In addition to the steps above, you might want to check that all the tests are passed.

To run all or some specific tests, please follow the guide [here](https://emmanuelhayford.com/7-ways-to-selectively-run-rspec-tests/).

The relevant test suites for this challenge is located at `spec/`. I have covered the model and the associated controller, plus a service used for calculating the payout for a time sheet entry.

### Running the app

You will need to start Rails server through the command `rails s` in a terminal.

Then please navigate to `localhost:3000/time_entries` and check out the app for yourself.

## Discussions
I used Tailwind and ReactJS to build the UI. On the back-end side, I opted to use Postgres' range types and added a GIST index to speed up the overlapping time entry validation.
